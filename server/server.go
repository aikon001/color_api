package main

import (
	"context"
	"log"
	"net"

	"google.golang.org/grpc"
	pb "gitlab.com/aikon001/color_api/colorapi"
)

const(
	port = ":50051"
)

type server struct {
	pb.UnimplementedColorsServer
}

func (s *server) GetColor (ctx context.Context, color *pb.Color) (*pb.Color,error) {
	return &pb.Color{Name:"Black",Rgb:0x000000},nil
}

func main() {
	lis, err := net.Listen("tcp",port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	pb.RegisterColorsServer(s,&server{})
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}